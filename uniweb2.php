<?php

function simplify($arr) {
	$out = [];
	foreach ($arr as $key => $value) {
		if (is_array($value)) {
			$tmp = simplify($value);
			foreach ($tmp as $key2 => $value2) {
				$out["$key.$key2"] = $value2;
			}
		}
		else {
			$out[$key] = $value;
		}
	}
	return $out;
}

// Example array
$a = [
	'a' => [
		'b' => 10,
		'c' => [
			'e' => 30,
			'f' => 40,
			'g' => [
				'h' => 5,
				'i' => 7,
			],
		],
		'd' => 50,
	],
	'k' => 2,
	'm' => [
		'n' => 60,
	]
];

$result = simplify($a);
print_r($result);
