CREATE TABLE IF NOT EXISTS `authors` (
  `author_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) NOT NULL,
  PRIMARY KEY (`author_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `books` (
  `book_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_title` text NOT NULL,
  `book_price` decimal(8,2) unsigned NOT NULL,
  PRIMARY KEY (`book_id`)
);

CREATE TABLE IF NOT EXISTS `catalog` (
  `book_id` int(10) unsigned DEFAULT NULL,
  `author_id` int(10) unsigned DEFAULT NULL,
  KEY `book_id` (`book_id`),
  KEY `author_id` (`author_id`)
);

-- демо-данные
INSERT INTO `authors` (`author_id`, `author_name`) VALUES
(1, 'Кинг С.'),
(2, 'Шевченко Т.'),
(3, 'Подервянский Л.');

INSERT INTO `books` (`book_id`, `book_title`, `book_price`) VALUES
(1, 'ффффффф', '4.00'),
(2, 'rsg', '3.00'),
(3, 'yyu', '3.44'),
(4, 'uuuu', '2.00'),
(5, 'ааабб', '7.00');

INSERT INTO `catalog` (`book_id`, `author_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 2),
(5, 2);

-- Список авторов, у которых больше 5 книг
SELECT * FROM AUTHORS t1 LEFT JOIN
  (SELECT count(*) book_count, author_id FROM catalog GROUP BY author_id) t2
  USING (author_id)
  WHERE book_count > 5;

-- Список авторов, у которых нет книг
SELECT * FROM AUTHORS t1 LEFT JOIN
  (SELECT count(*) book_count, author_id FROM catalog GROUP BY author_id) t2
  USING (author_id)
  WHERE book_count IS NULL;

-- Список авторов и количества книг у автора
SELECT * FROM AUTHORS t1 LEFT JOIN
  (SELECT count(*) book_count, author_id FROM catalog GROUP BY author_id) t2
  USING (author_id);